import { createRouter,createWebHashHistory } from "vue-router";
import Store from "./Storage";
const Router = createRouter({
    history:createWebHashHistory(),
    routes:[{path:'/login',component:()=>import ('../pages/login'),name:'login'},
            {path:'/home',component:()=>import ('../pages/home'),name:'home',children:[
                {path:'/home/category',component:()=>import('@/pages/home/Good/cate.vue'),name:'cate'},
                {path:'/home/order/0',meta:{type:0} ,component:()=>import ('@/pages/home/Order/common.vue')},
                {path:'/home/order/1',meta:{type:1},component:()=>import('@/pages/home/Order/secondsKill.vue')},
                {path:'/home/goods/:type', meta:{x:0},component:()=>import('@/pages/home/Good/common.vue')},
                {path:'/home/secondgoods/:type',meta:{x:1},component:()=>import('@/pages/home/Good/secondKill.vue')},
                {path:'/home/recommendgoods/:type',meta:{x:2},component:()=>import('@/pages/home/Good/recommend.vue')},
                {path:'/home/ownerlist',component:()=>import('@/pages/home/Manager/ownerlist.vue')},
                {path:'/home/ownerreq',component:()=>import('@/pages/home/Manager/ownerReq.vue')},
                {path:'/home/ownorder',component:()=>import('@/pages/home/Manager/ownerOrder.vue')},
                {path:'/home/tradeinfo',component:()=>import('@/pages/home/trade/tradeInfo.vue')},
                {path:'/home/tradelist',component:()=>import('@/pages/home/trade/tradelist.vue')},
                {path:'/home/data',component:()=>import('@/pages/home/data')}
            ]
                ,redirect:'/home/order/0'},
            {path:'/',redirect:'/login'}
        ]
})
Router.beforeEach((from)=>{
    let isLogin = Store.getters.isLogin;
    if(isLogin || from.name == 'login'){
        return true;
    }else{
      return {name:'login'}
    }
})
export default Router