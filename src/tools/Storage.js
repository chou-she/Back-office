import {createStore} from 'vuex'
const Store =createStore({
    mutations:{
        cleanUserInfo(state){
            state.userName="";
            state.userPassword=""
            this.commit('removeUserInfoStorage')
        },
        resigtUserInfo(state,{name,passWord}){
            state.userName=name;
            state.userPassword=passWord
            this.commit('userInfoStorage')
        },
        userInfoStorage(state){
            localStorage.setItem('USER',state.userName)
            localStorage.setItem('PWD',state.userPassword)
        },
        removeUserInfoStorage(state){
            localStorage.removeItem('USER')
            localStorage.removeItem('PWD')
        }
    },
    state(){
        return{
            userName:localStorage.getItem('USER') || "",
            userPassword:localStorage.getItem('PWD') ||  ""
        }
    },
    getters:{
        isLogin:(state)=>{
            return state.userName.length>0
        }
    }
})
export default Store