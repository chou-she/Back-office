import { result } from "lodash";
import mock from "mockjs";

mock.mock(RegExp('/mock/order' + ".*"),'get',(option)=>{
     const res =[]
    for (let i=0;i<mock.Random.integer(5,10);i++){
        res.push(mock.mock({           
                'name':option.url.split('?')[1].split('=')[1]==0?'普通商品':'秒杀商品',
                'price':mock.Random.integer(20,500)+'元',
                'buyer':mock.Random.cname(),
                'time':mock.Random.datetime('yyyy-MM-dd A HH:mm:ss'),
                'role':mock.Random.boolean(),
                'state':mock.Random.boolean(),
                'payType':mock.Random.boolean(),
                'source':mock.Random.url(),
                'phone':mock.mock(/\d{11}/)
            }))}
    const result ={
        status:200,
        message:'success',
        data:res
    }
    return result
 })
 mock.mock(RegExp('/mock/good' + ".*"),'get',(option)=>{
    const res =[]
   for (let i=0;i<mock.Random.integer(5,10);i++){  
        res.push(mock.mock({
           'name':option.url.split('?')[1].split('=')[1]==0?'普通商品':option.url.split('?')[1].split('=')[1]==1?'秒杀商品':'今日推荐',
            'img':mock.Random.dataImage('60x100','商品示例图'),
            'price':mock.Random.integer(20,500)+'元',
            'sellCount':mock.Random.integer(10,100),
            'count':mock.Random.integer(10,100),
            'back':mock.Random.integer(10,100),
            'backPrice':mock.Random.integer(0,5000)+'元'
            ,'owner':mock.Random.cname(),
            'time':mock.Random.datetime('yyyy-MM-dd A HH:mm:ss'),
            'state':mock.Random.boolean()
        }))
    }
    let result ={
        status:200,
        message:'success',
        data:res
    }
    return result
 })

 mock.mock('/mock/owner','get',(option)=>{
    const res =[]
   for (let i=0;i<mock.Random.integer(5,10);i++){
    res.push(mock.mock({
        'people':mock.Random.csentence(),
        'weixin':mock.Random.string(7,10),
        'state':mock.Random.boolean(),
        'income':'￥'+mock.Random.integer(0,500000),
        'back':'￥'+mock.Random.integer(0,1000),
        'backPrice':'￥'+mock.Random.integer(0,5000),
        'source':'站内',
        'customer':mock.Random.integer(0,50),
        'time':mock.Random.datetime('yyyy-MM-dd A HH:mm:ss')
    }))
}
        let result ={status:200,message:'success',data:res}
        return result
})

mock.mock('/mock/ownerreq','get',(option)=>{
    const res =[]
    for(let i=0;i<mock.Random.integer(5,10);i++){
        res.push(mock.mock({
            'people':mock.Random.csentence(),
            'state':mock.Random.boolean(),
            'updateTime':mock.Random.datetime('yyyy-MM-dd A HH:mm:ss'),
            'addTime':mock.Random.datetime('yyyy-MM-dd A HH:mm:ss'),
            isReady:mock.Random.boolean(),
        }))
    }
    let result ={res,status:200}
    return result
})
mock.mock('/mock/ownerorder','get',(option)=>{
    const res =[]
    for(let i=0;i<mock.Random.integer(5,10);i++){
        res.push(mock.mock({
            'order':'店长订单',
            'people':mock.Random.csentence(),
            'state':mock.Random.boolean(),
            'sendTime':mock.Random.datetime('yyyy-MM-dd A HH:mm:ss'),
            'payTime':mock.Random.datetime('yyyy-MM-dd A HH:mm:ss'),
            'ordernum':mock.Random.string(7,10),
            'name':mock.Random.cname(),
            'Num':mock.Random.integer(50,1000),
            'price':mock.Random.integer(10000,500000),
            'commission':mock.Random.integer(5000,50000),
        }))
    }
    let result ={data:res,status:200}
    return result
})
mock.mock('/mock/tradeinfo','get',(option)=>{
    const res =[]
    for(let i=0;i<mock.Random.integer(5,10);i++){
        res.push(mock.mock({
            'name':mock.Random.csentence(),
            'order':mock.Random.string(7,10),
            'username':mock.Random.cname(),
            'pay':mock.Random.boolean()?'线下支付':'网络支付',
            'payTime':mock.Random.datetime('yyyy-MM-dd A HH:mm:ss')
        }))
    }
    let result ={data:res,status:200}
    return result
})
mock.mock('/mock/tradelist','get',(option)=>{
    const res =[]
    for(let i=0;i<mock.Random.integer(5,10);i++){
        res.push(mock.mock({
            'notes':mock.Random.csentence(),
            'date':mock.Random.datetime('yyyy-MM-dd A HH:mm:ss'),
            'get':'￥'+mock.Random.integer(500,10000),
            'pay':'￥'+mock.Random.integer(500,10000),
        }))
    }
    let result ={data:res,status:200}
    return result
})
mock.mock('/mock/tradedata','get',(option)=>{
       return mock.mock({
            'allTra':mock.Random.integer(10000,50000),
            'speTra':mock.Random.integer(0,5000),
            'norTra':mock.Random.integer(0,5000),
            'userCount':mock.Random.integer(0,1000),
            'managerCount':mock.Random.integer(0,100),
            'time':mock.Random.datetime('yyyy-MM-dd A HH:mm:ss')
       })
    })

