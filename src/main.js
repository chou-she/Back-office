import { createApp } from 'vue'
import App from './App.vue'
import Router from './tools/Router'
import Store from './tools/Storage'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import './mock/mock'
import contentHeader from '@/components/header.vue'
import good from '@/components/good.vue'
import addGood from "@/components/addGood";
const app = createApp(App)
app.component('addGood',addGood)
app.component('contentHeader',contentHeader)
  app.component('good',good)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
  }
  app.config.warnHandler = () => null;
app.use(Router)
app.use(Store)
app.use(ElementPlus)
app.mount('#app')
