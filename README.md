# vue3-shop
## 效果预览图
![输入图片说明](src/assets/16687487073970.gif)
### 技术栈
-  :wave: 运用了vue3及elementPlus搭建的框架
-  :wave: 利用mock.js模拟接口，并用axios处理mockjs模拟的数据
- 例如--->
- `mock.mock(RegExp('/mock/order' + ".*"),'get',(option)=>{
     const res =[]
    for (let i=0;i<mock.Random.integer(5,10);i++){
        res.push(mock.mock({           
                'name':option.url.split('?')[1].split('=')[1]==0?'普通商品':'秒杀商品',
                'price':mock.Random.integer(20,500)+'元',
                'buyer':mock.Random.cname(),
                'time':mock.Random.datetime('yyyy-MM-dd A HH:mm:ss'),
                'role':mock.Random.boolean(),
                'state':mock.Random.boolean(),
                'payType':mock.Random.boolean(),
                'source':mock.Random.url(),
                'phone':mock.mock(/\d{11}/)
            }))}
    const result ={
        status:200,
        message:'success',
        data:res
    }
    return result
 })
`


- :wave: 利用了localStorage模拟token保持用户登录的状态
- :wave: 使用了echarts是部分数据可视化
- :wave: 封装组件提升开发效率